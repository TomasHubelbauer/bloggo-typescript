# TypeScript

- https://github.com/Microsoft/TypeScript-Handbook/issues/785#issuecomment-404520535

## Discriminated class union

[My example on GitHub](https://github.com/TomasHubelbauer/typescript-discriminated-class-union)

## Interesting Thingy #1

I was wondering is something like this was possible in TS:

```typescript
type Test =
    | { type: 'a'; a: boolean; }
    | { type: 'b'; b: number;}
    | { type: 'c'; c: string; d: string; }
    ;
test<t extends Test>() {
    // The switch should understand it is switching on `'a' | 'b' | 'c'` now
    // The switch should infer T to be the respective type union variant in the `case` branch
    // `as T` in the `case`s is for clarity also (it should be infered as per the above point)
    switch (foo(T).type) {
        case 'a': return barA() as T; // T is { type: 'a', a: boolean; }
        case 'b': return barB() as T; // T is { type: 'b', b: number; }
        case 'c': return barC() as T; // T is { type: 'c', c: string; d: string; }
        default: throw new Error(); // T is `never`
    }
}
```

I wasn't sure what `foo` would be here, not `typeof`.

But then I realized, if it's impossible in JavaScript, it's going to be impossible in TypeScript.
And we are not passing any type information in the actual runtime, consider this:

```javascript
test() {
    switch (foo(T).type) {
    
    }
}
```

There is no place the `T` could come from, in JS, it's all but forgotten.
If we did pass an instance of `T` as a parameter, then it could work, but that didn't suit my design,
so I made a different choice in the end.

## Importing images

1. `index.d.ts`: `declare module '*.png';`
2. `index.tsx`: `/// <reference path='./index.d.ts'/>`
3. `index.tsx`: `import checkerboard from './img/checkerboard.png';`

## `@connect` decorator

https://github.com/TomasHubelbauer/react-redux-typescript-connect-decorator-demo

## *Cannot find type `AsyncIterableIterator`*

Add `esnext.asynciterable` to `lib` in `tsconfig.json`.

## Component Replacing

`baseUrl` (relative to the repository directory) can be set which will allow relative imports like `things/Something.tsx`
where `things` is just a directory relative to `baseUrl`.

`paths` can then be used in a case where `a` and `b` is both used and we want to preserve `a` and only duplicate files
that will be changed in `b` by redirecting some of the link in `a` to `b`.

## Fake Module Definition

```typescript
import * as x from 'x'; // Untyped module
```

`x.d.ts`:

```typescript
declare module 'x';
```

